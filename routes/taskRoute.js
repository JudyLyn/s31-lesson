//Routes - contain all the endpoints for our application
//Instead of putting the routes in index.js, we seperate the routes such that index.js only contains information on the server. with this, we need to use express Router() function

const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController')

//Route to get all the tasks
router.get("/", (req, res)=>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})
//Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));

})
//route to update a task based on task ID passed in as a URL parameter denoted by :id
router.put("/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})
//delete a task
router.delete("/:id", (req, res) =>{
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})



//Update a task, change its status to "complete"
router.put("/:id/complete", (req, res) => {
	taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
})




module.exports = router;