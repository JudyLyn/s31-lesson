//1. Create a directory named s31. Navigate to this directory.

//2. Initialize NPM in this package via the terminal command:
//npm init -y

//3. Install Express.js and Mongoose via the terminal command:
//npm install express mongoose nodemon

//4. Create a file named index.js. This will serve as our application's entry point. Code it as follows:


//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");
//With this, we could use all the routes in taskRoute.js

//database connection
mongoose.connect("mongodb+srv://database-admin:admin_1234@sampledb.zxc7v.mongodb.net/b84_todo?retryWrites=true&w=majority", {useNewUrlParser:true});
mongoose.connection.once("open", ()=>console.log("Now connected to the database"));

//server setup
const app = express()
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//add the task route
app.use("/tasks", taskRoute); //By doing this, all the task routes would start with /tasks

//server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

/*====================================================*/

//5.Create a directory named 'models' at the project's root directory. In this folder, create a file named task.js. Define it as follows:

//Create the Schema, model and export the file
const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
});

module.exports = mongoose.model("Task", taskSchema);
//module.exports is a way for node to treat this value as a "package" that can be used by other files

/*====================================================*/


//6. We imported taskRoute in our app entry point via the require() directive. However, this does not exist yet.
// At the root of our project directory, create a routes folder. In it, create a taskRoute.js file. Let's define our routes as follows:

//Routes - contain all the endpoints for our application
//Instead of putting the routes in index.js, 
//we separate the routes such that index.js only contains information on the server
//With this, we need to use express' Router() function
const express = require("express");
const router = express.Router();// to create a modular, mountable route handlers. we used to create a new router object to handle requests
const taskController = require("../controllers/taskController");
//the taskController allows us to use the controller's functions

//Route to get all the tasks
router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
	//Note that there is no process that happens in the routes, it just calls the needed function from the controller. All the processing happens in the controller.
})

//.then waits the getAllTasks() to finish before sending the result
//.then method returns a promise. It is used to deal with asynchrounous task

//Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	//the createTask needs the data from the body, so we need to supply it to the function
})

//route to update a task based on task ID passed in as a URL parameter denoted by :id
//delete a task

module.exports = router;//we have a newlly created module
//module is a variable that represents the current module and export it as an object.
//anything you assign to module.exports will be reused throught out the application



//note: back to number 4, sa index.js, and require the routes
/*====================================================*/

//7. You can see in the taskRoute.js file that we required a taskController module. This controller will contain all the logic for carrying out operations involving the task resource. At the project root directory, create a folder named controllers. In it, create a taskController.js file and define it as follows:

//Controllers contain the functions of your application
//Meaning all the operations it can do will be placed in this folder

//Use the model file created earlier
const Task = require("../models/task"); //This says that it uses the contents of the task.js file in the model folder
//Meaning it uses the Task model

//Create the functions and export these functions
//get all tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}
//.then can be used to wait for the find() to finish before sending the result.

module.exports.createTask = (body) => {
	let newTask = new Task({
		name : body.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false; //save unsuccessful
		} else {
			return task; //save successful, return new task
		}
	})
	//.then waits until the saving is complete before running the function to return true or false

	//The return false / return task only says to the newTask.save() whether the save was sucessful or not.
	//However, newTask.save was not able to tell createTask whether the saving was successful
	//In the real world the analogy is this: The person who was in the kitchen was able to make your order and told it to your cashier, however, your cashier was not able to pick up the order and give it to you.
	//That's why, we need to add the return to newTask.save() so it can tell createTask that the saving was successful.
}


/*====================================================*/

//8. Postman
/*
Add task
POST
localhost:4000/tasks
{
	"name": "sleep"
}
*/

/*
Get all tasks
Get
localhost:4000/tasks

*/

/*====================================================*/

//9. //route to update a task based on task ID passed in as a URL parameter denoted by :id
//routes > taskRoutes.js
router.put("/:id", (req, res) => {
	//:id is a wildcard where you can put any value, it then creates a link between id and the value
		//Ex. localhost:3000/tasks/1234 -> the value of 1234 is assigned to id.
	//URL parameter values are accessed via the req.params object. The property name of this object will match the given URL parameter name (id in our case)
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


/*====================================================*/



//10. Go to controllers
//controllers > taskController.js

//taskId is the URL parameter id passed in from taskRoute.js
//newContent is the req.body passed in from taskRoute.js
module.exports.updateTask = (taskId, newContent) => {
	//1. get the task with the id using findById
	//2. replace the task's name with the name from the body
	//3. save the task
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(err);
			return false;
		}
		//results of the findById will be placed in result
		result.name = newContent.name;
		/*result.status = newContent.status*/
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}

//deprecation warning
//useFindAndModify: false


/*
Postman:
PUT
localhost:4000/tasks/:id
{
	"name": "sleep edited"
}
*/


/*====================================================*/


//11. //Delete a task
//Routes > taskRoute.js
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


//12. controllers > taskController.js

module.exports.deleteTask = (taskId) => {
	//1. look for the task with the corresponding id
	//2. use the remove() to delete the task from the database
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}

/*
Postman:
delete a task
DELETE
localhost:4000/tasks/:id
*/


/*Activity*/
//1. define the controller action and route for retrieving a single task based on its ID sent via URL parameters.
//2. Define the controller action and route for setting a task's status as complete when a PUT request is received at the /:id/complete endpoint.