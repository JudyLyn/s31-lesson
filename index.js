//4. Set up dependencies
const express = require('express');
const mongoose = require('mongoose');
const taskRoute = require('./routes/taskRoute')

//database connection
mongoose.connect('mongodb+srv://dbUser:dbUser@zuitt.ri5rh.mongodb.net/sample?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});

mongoose.connection.once('open', () => console.log("Now connected to the database"));

//server setup
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//add the task route
app.use("/tasks", taskRoute); //By doing this, all the task routes would start with /tasks
//localhost:4000/tasks

//server listening
app.listen(port, () => console.log(`Now listening to port ${port}`))

