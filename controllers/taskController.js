//Controllers contain the functions of your application, meaning all the operations it can do will be placed in this folder

const Task = require('../models/task')


module.exports.getAllTasks = () =>{
	return Task.find({}).then(result => {
		return result
	})
}
//.then can be used to wait for the find() to finish before sending the result


module.exports.createTask = (body) => {
	let newTask = new Task({
		name: body.name
	});

	return newTask.save().then((task, error)=>{
		if(error){
			console.log(error)
			return false; // save unsuccessfuly
		}else{
			return task; //save successfull, return new task
		}
	})
}
//.then waits until the saving is complete before running the function to return true or false
//the return false / return task only says to the newTask.save() wheter the save was successful or not.


module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(err);
			return false;
		}

		result.name = newContent.name;
		result.status = newContent.status
		return result.save().then((updatedTask, saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}else{
				return updatedTask
			}
		})
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}



module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return err;
		}
		result.status = "complete";
		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return saveErr;
			} else {
				return updatedTask;
			}
		})
	})
}
